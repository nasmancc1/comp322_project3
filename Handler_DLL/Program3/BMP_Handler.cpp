/* 
 * Author: Caleb Nasman
 * COMP 342
 * This file implements BMP_Handler.h and reads and writes bmp files
 */

#include "BMP_Handler.h"
#include "BMP_Header.h"
#include <fstream>
#include <iostream>
#pragma pack(1)

using namespace std;

// load BMP file
unsigned char* BMP_Handler::loadBMP(const char* filename, int& width, int& height) {
	ifstream fin(filename, ios::binary);
	file_header fh;
	image_header ih;
	unsigned char* vals;

	// read in meta data
	fin.read(reinterpret_cast<char*>(&fh), sizeof(file_header));
	fin.read(reinterpret_cast<char*>(&ih), sizeof(image_header));

	// calculate padding and dimensions
	width = ih.biWidth;
	height = ih.biHeight;
	// padding is added to each row to keep the length a multiple of 4
	int padding = (4 - ((3 * width) % 4)) % 4;
	vals = new unsigned char[3*width*height];

	//read in data
	for (int i = 0; i < height; i++) {
		// since each pixel is 3 bytes long, we loop to 3 times the pixel width
		for (int j = 0; j < 3 * width; j++) {
			char ch;
			fin.get(ch);
			vals[j + i * 3 * width] = ch;
		}
		// need to throwaway padding that is at the end of each row
		char temp;
		for (int k = 0; k < padding; k++) {
			fin.get(temp);
		}
	}
	fin.close();
	return vals;
}

// save the BMP file
void BMP_Handler::saveBMP(const char* filename, const unsigned char* pixels, int width , int height) {
	ofstream fout(filename, ios::binary);
	file_header fh;
	image_header ih;

	// calculate padding
	// padding is added to each row to keep the length a multiple of 4
	int padding = (4 - ((3 * width) % 4)) % 4;
	// the size of the headers is 54 bytes long, and each pixel is 3 bytes
	fh.bfSize = 54 + (3 * width + padding) * height;
	
	// set the metadata for the file and image headers
	ih.biSize = 40;
	ih.biWidth = width;
	ih.biHeight = height;
	ih.biBitCount = 24;
	ih.biSizeImage = ((3 * width) + padding) * height;

	// write meta data
	fout.write(reinterpret_cast<char*>(&fh), sizeof(file_header));
	fout.write(reinterpret_cast<char*>(&ih), sizeof(image_header));

	// write piwidthel data
	for (int i = 0; i < height; i++) {
		// since each pixel is 3 bytes long, we loop to 3 times the pixel width
		for (int j = 0; j < 3*width; j++) {
			fout.put(pixels[i*3*width + j]);
		}
		// need to throwaway padding that is at the end of each row
		char temp = 0;
		for (int k = 0; k < padding; k++)
			fout.put(temp);
	}

	fout.close();
}