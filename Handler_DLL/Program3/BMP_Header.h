/*
* Author: Caleb Nasman
* COMP 342
* This file defines structs for the file and image headers in bmp files 
*/

#pragma once

// these structs contain the metadata found in standard file and image header with 24 bit bmp
// consult http://www.dragonwins.com/domains/GetTechEd/bmp/bmpfileformat.htm for details on the structure of bmp files

struct file_header {
	unsigned short bfType = 0x4D42; // the magic number
	unsigned int bfSize; // the size of the bmp header
	unsigned int bfReserved = 0;
	unsigned int bfOffBits = 54; // offset to data
};

struct image_header {
	unsigned int biSize;
	unsigned int biWidth; 
	unsigned int biHeight;
	unsigned short biPlanes = 1;
	unsigned short biBitCount;
	unsigned int biCompression = 0; // no compression
	unsigned int biSizeImage;
	unsigned int biXPelsPerMeter = 0;
	unsigned int biYPelsPerMeter = 0;
	unsigned int biClrUsed = 0;
	unsigned int biClrImportant = 0;
};