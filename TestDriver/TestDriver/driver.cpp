#include <iostream>
#include "BMP_Handler.h"

using std::cout;
using std::cin;
using std::endl;

int main() {
	// file names of in and out files
	char in[100], out[100];

	// image data
	unsigned char *pixels;
	int width, height;

	cout << "Enter filename of bmp file to load: ";
	cin >> in;
	pixels = BMP_Handler::loadBMP(in, width, height);
	cout << "Enter filename of bmp file to save: ";
	cin >> out;
	BMP_Handler::saveBMP(out, pixels, width, height);
	cout << "program exitted sucessfully" << endl;
}